package lili.service.impl;

import org.springframework.stereotype.Service;

import lili.api.data.SimulatorData;
import lili.api.data.ThermostatData;
import lili.service.ThermostatService;
import lili.util.DateUtil;

@Service
public class ThermostatServiceImpl implements ThermostatService {

	@Override
	public Integer refreshTemp() {
		if (!ThermostatData.onoff)
			return -1;
		
		long t = SimulatorData.curTimestamp;

		int date = DateUtil.getDateString(t);
		Double curPowerConsumption = ThermostatData.powerConsumption
				.get(date);
		if (curPowerConsumption == null)
			curPowerConsumption = 0d;
		
		ThermostatData.powerConsumption.put(date, curPowerConsumption
				+ ThermostatData.REFRESH_INTERVAL / 3600 * 1.0);

		if (ThermostatData.override) {
			return ThermostatData.temp;
		} else {
			if (DateUtil.isWorkday(t) && DateUtil.isDaytime(t))
				return ThermostatData.presetWorkdayDaytime;

			else if (DateUtil.isWorkday(t) && !DateUtil.isDaytime(t))
				return ThermostatData.presetWorkdayNighttime;

			else if (!DateUtil.isWorkday(t) && DateUtil.isDaytime(t))
				return ThermostatData.presetWeekendDaytime;

			else
				// (!DateUtil.isWorkday(t) && !DateUtil.isDaytime(t))
				return ThermostatData.presetWeekendNighttime;
		}
	}

	@Override
	public void setTemp(int tempToSet) {
		ThermostatData.temp = tempToSet;
	}

	@Override
	public void setTempOverride(boolean override) {
		ThermostatData.override = override;
	}

	@Override
	public void setOnOff(boolean onoff) {
		ThermostatData.onoff = onoff;
	}

	@Override
	public double getPowerConsumption(int dayOfYear) {
		return ThermostatData.powerConsumption.get(dayOfYear);
	}

}
