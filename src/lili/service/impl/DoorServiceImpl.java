package lili.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lili.dao.DoorDao;
import lili.dto.DoorViewInfo;
import lili.pojo.Door;
import lili.service.DoorService;

@Service
public class DoorServiceImpl implements DoorService {

	@Autowired
	DoorDao doorDao;

	@Transactional
	public List<DoorViewInfo> getListDoorViewInfo() {
		List<Door> listDoor = doorDao.getListDoor();
		List<DoorViewInfo> result = new ArrayList<DoorViewInfo>();
		for (Door door : listDoor)
			result.add(toDoorViewInfo(door));
		return result;
	}

	private DoorViewInfo toDoorViewInfo(Door door) {
		DoorViewInfo doorViewInfo = new DoorViewInfo();
		doorViewInfo.setDoorId(door.getDoorId());
		doorViewInfo.setName(door.getName());
		doorViewInfo.setClosed(door.isClosed());
		return doorViewInfo;
	}
}
