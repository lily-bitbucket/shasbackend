package lili.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lili.dao.FishDao;
import lili.dto.FishViewInfo;
import lili.pojo.Fish;
import lili.service.FishService;

@Service
public class FishServiceImpl implements FishService {

	@Autowired
	FishDao fishDao;

	@Transactional
	public List<FishViewInfo> getListFishViewInfo() {
		List<Fish> listFish = fishDao.getListFish();
		List<FishViewInfo> result = new ArrayList<FishViewInfo>();
		for (Fish fish : listFish)
			result.add(toFishViewInfo(fish));
		return result;
	}

	private FishViewInfo toFishViewInfo(Fish fish) {
		FishViewInfo fishViewInfo = new FishViewInfo();
		fishViewInfo.setFishId(fish.getFishId());
		fishViewInfo.setName(fish.getName());
		return fishViewInfo;
	}

}
