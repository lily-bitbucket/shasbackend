package lili.service.impl;

import org.springframework.stereotype.Service;

import lili.api.data.SimulatorData;
import lili.service.SimuatorService;

@Service
public class SimulatorServiceImpl implements SimuatorService {

	@Override
	public void setTime(long timestamp) {
		SimulatorData.curTimestamp = timestamp;
		notify();
	}

	@Override
	public void setOutSideTemp(long outsideTemp) {
		SimulatorData.outsideTemp = outsideTemp;
	}
}
