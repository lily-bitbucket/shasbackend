package lili.service;

public interface SimuatorService {

	void setTime(long timestamp);

	void setOutSideTemp(long outsideTemp);
	
}
