package lili.service;

public interface ThermostatService {

	Integer refreshTemp();

	void setTemp(int tempToSet);

	void setTempOverride(boolean override);

	void setOnOff(boolean onoff);

	double getPowerConsumption(int dayOfYear);

}
