package lili.service;

import java.util.List;

import lili.dto.FishViewInfo;

public interface FishService {

	List<FishViewInfo> getListFishViewInfo();

}
