package lili.service;

import java.util.List;

import lili.dto.DoorViewInfo;

public interface DoorService {

	List<DoorViewInfo> getListDoorViewInfo();

}
