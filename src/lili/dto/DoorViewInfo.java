package lili.dto;

import java.io.Serializable;

public class DoorViewInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	private long doorId;
	private String name;
	private boolean closed;

	public long getDoorId() {
		return doorId;
	}

	public void setDoorId(long doorId) {
		this.doorId = doorId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isClosed() {
		return closed;
	}

	public void setClosed(boolean closed) {
		this.closed = closed;
	}
}
