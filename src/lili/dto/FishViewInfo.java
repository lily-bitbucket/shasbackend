package lili.dto;

import java.io.Serializable;

public class FishViewInfo implements Serializable {

	private static final long serialVersionUID = 1L;

	private long fishId;
	private String name;

	public long getFishId() {
		return fishId;
	}

	public void setFishId(long fishId) {
		this.fishId = fishId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
