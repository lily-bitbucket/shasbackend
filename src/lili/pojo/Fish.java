package lili.pojo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "fish")
public class Fish implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "fish_id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long fishId;

	@Column(name = "name")
	private String name;

	public Long getFishId() {
		return fishId;
	}

	public void setFishId(Long fishId) {
		this.fishId = fishId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

}
