package lili.dao;

import java.util.List;

import lili.pojo.Fish;

public interface FishDao {

	List<Fish> getListFish();

}
