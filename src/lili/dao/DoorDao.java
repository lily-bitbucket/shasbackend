package lili.dao;

import java.util.List;

import lili.pojo.Door;

public interface DoorDao {

	List<Door> getListDoor();

}
