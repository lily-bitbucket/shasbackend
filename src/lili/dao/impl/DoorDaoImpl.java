package lili.dao.impl;

import java.util.Collections;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import lili.dao.DoorDao;
import lili.pojo.Door;

@Repository
public class DoorDaoImpl implements DoorDao {

	@Autowired
	private SessionFactory sessionFactory;

	private static final Logger logger = LoggerFactory
			.getLogger(DoorDaoImpl.class);

	@SuppressWarnings("unchecked")
	@Override
	public List<Door> getListDoor() {
		try {
			Query q = sessionFactory.getCurrentSession().createQuery(
					"from Door");

			return (List<Door>) q.list();
		} catch (Exception e) {
			logger.error("Failed to get a list of doors", e);
		}
		return Collections.emptyList();
	}

}
