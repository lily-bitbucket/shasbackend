package lili.dao.impl;

import java.util.Collections;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import lili.dao.FishDao;
import lili.pojo.Fish;

@Repository
public class FishDaoImpl implements FishDao {

	@Autowired
	private SessionFactory sessionFactory;

	private static final Logger logger = LoggerFactory
			.getLogger(FishDaoImpl.class);

	@SuppressWarnings("unchecked")
	@Override
	public List<Fish> getListFish() {
		try {
			Query q = sessionFactory.getCurrentSession().createQuery(
					"from Fish");

			return (List<Fish>) q.list();
		} catch (Exception e) {
			logger.error("Hibernate failed to get a list of fishes", e);
		}
		return Collections.emptyList();
	}

}
