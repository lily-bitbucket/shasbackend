package lili.api;

import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import lili.service.ThermostatService;
import lili.util.ResponseUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Path("/thermostat")
@Component
public class ThermostatHandler {
	
	@Autowired
	private ThermostatService thermostatService;
	
	@GET
	@Path("/temp")
	@Produces(MediaType.APPLICATION_JSON)
	public Response refreshTemp() {
		Integer temp = thermostatService.refreshTemp();
		return ResponseUtil.createJsonResponse(Status.OK, temp);
	}
	
	@GET
	@Path("/powerconsumption/{dayOfYear}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getPowerConsumption(@PathParam("dayOfYear") int dayOfYear) {
		double powerConsumption = thermostatService.getPowerConsumption(dayOfYear);
		return ResponseUtil.createJsonResponse(Status.OK, powerConsumption);
	}
	
	@PUT
	@Path("/temp/{tempToSet}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response setTemp(@PathParam("tempToSet") int tempToSet) {
		thermostatService.setTemp(tempToSet);
		return ResponseUtil.createJsonResponse(Status.OK);
	}
	
	@PUT
	@Path("/override/{override}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response setTempOverride(@PathParam("override") boolean override) {
		thermostatService.setTempOverride(override);
		return ResponseUtil.createJsonResponse(Status.OK);
	}
	
	@PUT
	@Path("/onoff/{onoff}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response setOnOff(@PathParam("onoff") boolean onoff) {
		thermostatService.setOnOff(onoff);
		return ResponseUtil.createJsonResponse(Status.OK);
	}
}
