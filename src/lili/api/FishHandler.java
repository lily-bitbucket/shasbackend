package lili.api;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lili.dto.FishViewInfo;
import lili.service.FishService;
import lili.util.ExceptionUtil;
import lili.util.ResponseUtil;

@Path("/fishes")
@Component
public class FishHandler {

	@Autowired
	FishService fishService;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getListFishViewInfo(@PathParam("param") String msg) {
		try {
			List<FishViewInfo> listFishViewInfo = fishService
					.getListFishViewInfo();

			return ResponseUtil.createJsonResponse(Status.OK, listFishViewInfo);
		} catch (Exception e) {
			return ExceptionUtil.handleExceptionWithJSONResponse(e);
		}
	}
}
