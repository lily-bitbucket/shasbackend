package lili.api.data;

import java.util.HashMap;
import java.util.Map;

public class ThermostatData {

	public static final int REFRESH_INTERVAL = 2;

	public static int temp;

	public static int presetWorkdayDaytime = 75;
	public static int presetWorkdayNighttime = 70;
	public static int presetWeekendDaytime = 70;
	public static int presetWeekendNighttime = 70;

	public static boolean override;
	public static boolean onoff;

	public static Map<Integer, Double> powerConsumption = new HashMap<Integer, Double>();
}
