package lili.api;

import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lili.service.SimuatorService;
import lili.util.ResponseUtil;

@Path("/simulator")
@Component
public class SimulatorHandler {
	
	@Autowired
	private SimuatorService simulatorService;
	
	@PUT
	@Path("/time/{timestamp}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response setTime(@PathParam("timestamp") long timestamp) {
		simulatorService.setTime(timestamp);
		return ResponseUtil.createJsonResponse(Status.OK);
	}
	
	@PUT
	@Path("/outsidetemp/{outsideTemp}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response setOutsideTemp(@PathParam("outsideTemp") long outsideTemp) {
		simulatorService.setOutSideTemp(outsideTemp);
		return ResponseUtil.createJsonResponse(Status.OK);
	}
}
