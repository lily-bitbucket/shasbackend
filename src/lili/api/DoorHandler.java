package lili.api;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import lili.dto.DoorViewInfo;
import lili.service.DoorService;
import lili.util.ExceptionUtil;
import lili.util.ResponseUtil;

@Path("/doors")
@Component
public class DoorHandler {

	@Autowired
	DoorService doorService;

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Response getListDoorViewInfo(@PathParam("param") String msg) {
		try {
			List<DoorViewInfo> listDoorViewInfo = doorService
					.getListDoorViewInfo();

			return ResponseUtil.createJsonResponse(Status.OK, listDoorViewInfo);
		} catch (Exception e) {
			return ExceptionUtil.handleExceptionWithJSONResponse(e);
		}
	}
}
