package lili.util;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class ExceptionUtil {

	private static final Logger logger = LoggerFactory
			.getLogger(ExceptionUtil.class);

	public static Response handleExceptionWithJSONResponse(
			Exception exceptionToHandle) {
		try {
			logger.error(exceptionToHandle.getMessage(), exceptionToHandle);
			throw exceptionToHandle;
		} catch (Exception e) {
			return ResponseUtil.createJsonResponse(Status.BAD_REQUEST);
		}
	}
}
