package lili.util;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

public class ResponseUtil {
	public static Response createJsonResponse(Status status) {
		return Response.status(status).type(MediaType.APPLICATION_JSON)
				.entity(status).build();
	}

	public static Response createJsonResponse(Status status, Object data) {
		return Response.status(status).type(MediaType.APPLICATION_JSON)
				.entity(data).build();
	}
}
