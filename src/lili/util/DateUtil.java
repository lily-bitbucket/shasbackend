package lili.util;

import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;

public class DateUtil {

	public static boolean isWorkday(long curTimestamp) {
		DateTime dateTime = new DateTime(curTimestamp);
		int dayOfWeek = dateTime.getDayOfWeek();
		if (dayOfWeek == DateTimeConstants.SATURDAY
				|| dayOfWeek == DateTimeConstants.SUNDAY)
			return false;
		return true;
	}

	public static boolean isDaytime(long curTimestamp) {
		DateTime dateTime = new DateTime(curTimestamp);
		int hour = dateTime.getHourOfDay();
		return ((hour >= 9) && (hour <= 17));
	}

	public static int getDateString(long t) {
		DateTime dateTime = new DateTime(t);
		return dateTime.getDayOfYear();
	}
}
